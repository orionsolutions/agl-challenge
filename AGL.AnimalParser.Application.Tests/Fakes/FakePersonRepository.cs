﻿using AGL.AnimalParser.Data.Repositories;
using AGL.AnimalParser.Models.Dtos;
using AGL.AnimalParser.Models.Enums;
using AGL.AnimalParser.Models.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AGL.AnimalParser.Application.Tests.Fakes
{
    internal class FakePersonRepository : IPersonRepository
    {
        public FakePersonRepository(List<PersonDto> people)
        {
            _people = people;
        }

        private readonly IReadOnlyList<PersonDto> _people;

        public async Task<List<PersonDto>> GetAllPeople()
        {
            return await Task.FromResult(_people.ToList());
        }

        public HashSet<PetType> GetAllPetTypes()
        {
            return EnumHelper.GetAllEnumValues<PetType>();
        }
    }
}