using AGL.AnimalParser.Application.Services;
using AGL.AnimalParser.Application.Tests.Fakes;
using AGL.AnimalParser.Models.Dtos;
using AGL.AnimalParser.Models.Enums;
using AGL.AnimalParser.Models.Helpers;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AGL.AnimalParser.Application.Tests
{
    public class PersonAppServiceTests
    {
        [Fact]
        public async Task GroupAllPetsByGender_SingleGender_MixedPetTypes()
        {
            // Arrange
            PetType type = PetType.Cat;
            var service = GetAppService(new List<PersonDto>()
            {
                new PersonDto()
                {
                    Name = "John",
                    Age = 20,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Johnny", Type = PetType.Cat} }
                },
                new PersonDto()
                {
                    Name = "Geoff",
                    Age = 25,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Geoffy", Type = PetType.Cat} }
                },
                new PersonDto()
                {
                    Name = "Alison",
                    Age = 30,
                    Gender = PersonGender.Female,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Alisony", Type = PetType.Dog} }
                }
            });

            // Act
            var groups = await service.GroupAllPetsByGenderAndOrderByPetName(type);

            // Assert
            Assert.True(groups.ContainsKey(PersonGender.Male), "Did not find male cat owners.");
            Assert.True(groups.ContainsKey(PersonGender.Female), "Did not return list female pet owners.");
            Assert.NotNull(groups[PersonGender.Female]);
            Assert.True(groups[PersonGender.Female].Count == 0, "Did not return empty list female pet owners.");
            Assert.True(groups[PersonGender.Male].Count.Equals(2), "Did not find all cat owners.");
            Assert.True(groups.All(group => group.Value.All(pet => pet.Type == type)), "Did not filter out dog owners.");
        }

        [Fact]
        public async Task GroupAllPetsByGender_SingleGender_MixedPetTypes_PetNameOrderingAscending()
        {
            // Arrange
            PetType type = PetType.Cat;
            var service = GetAppService(new List<PersonDto>()
            {
                new PersonDto()
                {
                    Name = "John",
                    Age = 20,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Johnny", Type = PetType.Cat} }
                },
                new PersonDto()
                {
                    Name = "Geoff",
                    Age = 25,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Geoffy", Type = PetType.Cat} }
                },
                new PersonDto()
                {
                    Name = "James",
                    Age = 30,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Jamesy", Type = PetType.Cat} }
                }
            });

            // Act
            var groups = await service.GroupAllPetsByGenderAndOrderByPetName(type);

            // Assert
            Assert.True(groups[PersonGender.Male][0].Name.Equals("Geoffy"), "Did not order pet names correctly within male group.");
            Assert.True(groups[PersonGender.Male][1].Name.Equals("Jamesy"), "Did not order pet names correctly within male group.");
            Assert.True(groups[PersonGender.Male][2].Name.Equals("Johnny"), "Did not order pet names correctly within male group.");
        }

        [Fact]
        public async Task GroupAllPetsByGender_MixedGenders_SinglePetType()
        {
            // Arrange
            PetType type = PetType.Dog;
            var service = GetAppService(new List<PersonDto>()
            {
                new PersonDto()
                {
                    Name = "John",
                    Age = 20,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Johnny", Type = PetType.Dog} }
                },
                new PersonDto()
                {
                    Name = "Geoff",
                    Age = 25,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Geoffy", Type = PetType.Dog} }
                },
                new PersonDto()
                {
                    Name = "Alison",
                    Age = 30,
                    Gender = PersonGender.Female,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Alisony", Type = PetType.Dog} }
                },
                new PersonDto()
                {
                    Name = "Ben",
                    Age = 26,
                    Gender = PersonGender.Male,
                    Pets = new List<PetDto>() { new PetDto() { Name = "Benny", Type = PetType.Cat} }
                }
            });

            // Act
            var groups = await service.GroupAllPetsByGenderAndOrderByPetName(type);

            // Assert
            Assert.True(groups.ContainsKey(PersonGender.Male), "Did not find male dog owners.");
            Assert.True(groups.ContainsKey(PersonGender.Female), "Did not find female dog owners.");
            Assert.True(groups[PersonGender.Male].Count.Equals(2), "Did not find all male dog owners.");
            Assert.True(groups[PersonGender.Female].Count.Equals(1), "Did not find all female dog owners.");
            Assert.True(groups.All(group => group.Value.All(pet => pet.Type == type)), "Did not filter cat owners.");
        }

        [Fact]
        public async Task GroupAllPetsByGender_HandleEmptyDataSource()
        {
            // Arrange
            PetType type = PetType.Dog;
            var service = GetAppService(new List<PersonDto>());

            // Act
            var groups = await service.GroupAllPetsByGenderAndOrderByPetName(type);

            // Assert
            Assert.NotNull(groups);
            Assert.NotEmpty(groups);
        }

        [Fact]
        public async Task GroupAllPetsByGender_ContainsAllGenders()
        {
            // Arrange
            PetType type = PetType.Dog;
            var service = GetAppService(new List<PersonDto>());

            // Act
            var groups = await service.GroupAllPetsByGenderAndOrderByPetName(type);

            // Assert
            Assert.NotNull(groups);
            foreach (var gender in EnumHelper.GetAllEnumValues<PersonGender>())
            {
                Assert.True(groups.ContainsKey(gender), $"Did not contain empty list of {gender}.");
            }
        }

        [Fact]
        public void GetAllPetTypes_FindAllEnumValues()
        {
            // Arrange
            var service = GetAppService(new List<PersonDto>());

            // Act
            var petTypes = service.GetAllPetTypes();

            // Assert
            Assert.NotNull(petTypes);
            Assert.Equal(Enum.GetNames(typeof(PetType)).Length, petTypes.Count);
        }

        private IPersonAppService GetAppService(List<PersonDto> people)
        {
            return new PersonAppService(new FakePersonRepository(people), new LoggerConfiguration().CreateLogger());
        }
    }
}