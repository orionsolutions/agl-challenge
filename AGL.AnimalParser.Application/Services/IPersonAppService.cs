﻿using AGL.AnimalParser.Models.Dtos;
using AGL.AnimalParser.Models.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGL.AnimalParser.Application.Services
{
    public interface IPersonAppService
    {
        Task<Dictionary<PersonGender, List<PetDto>>> GroupAllPetsByGenderAndOrderByPetName(PetType groupByPetType);

        HashSet<PetType> GetAllPetTypes();
    }
}