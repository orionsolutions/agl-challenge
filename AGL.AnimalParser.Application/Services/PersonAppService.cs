﻿using AGL.AnimalParser.Data.Repositories;
using AGL.AnimalParser.Models.Dtos;
using AGL.AnimalParser.Models.Enums;
using AGL.AnimalParser.Models.Helpers;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AGL.AnimalParser.Application.Services
{
    public class PersonAppService : IPersonAppService
    {
        public PersonAppService(IPersonRepository personRepository, ILogger logger)
        {
            _personRepository = personRepository;
            _logger = logger.ForContext<PersonAppService>();
        }

        private readonly ILogger _logger;

        private readonly IPersonRepository _personRepository;

        public async Task<Dictionary<PersonGender, List<PetDto>>> GroupAllPetsByGenderAndOrderByPetName(PetType groupByPetType)
        {
            _logger.Verbose("Grouping all {PetType} pets by genders.", groupByPetType);

            var withPets = from person in await _personRepository.GetAllPeople()
                           where person.Pets != null
                           where person.Pets.Any(pet => pet.Type == groupByPetType)
                           group person.Pets by person.Gender into grouped
                           select new
                           {
                               Gender = grouped.Key,
                               Pets = grouped.SelectMany(g => g).Where(g => g.Type == groupByPetType).OrderBy(g => g.Name).ToList()
                           };

            var output = withPets.ToDictionary(p => p.Gender, p => p.Pets);

            // Send empty list for all genders
            foreach (PersonGender gender in EnumHelper.GetAllEnumValues<PersonGender>())
            {
                if (!output.ContainsKey(gender))
                    output.Add(gender, new List<PetDto>());
            }

            return output;
        }

        public HashSet<PetType> GetAllPetTypes()
        {
            _logger.Verbose("Getting all pet types.");
            return _personRepository.GetAllPetTypes();
        }
    }
}