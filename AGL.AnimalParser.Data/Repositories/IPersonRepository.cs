﻿using AGL.AnimalParser.Models.Dtos;
using AGL.AnimalParser.Models.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGL.AnimalParser.Data.Repositories
{
    public interface IPersonRepository
    {
        Task<List<PersonDto>> GetAllPeople();

        HashSet<PetType> GetAllPetTypes();
    }
}