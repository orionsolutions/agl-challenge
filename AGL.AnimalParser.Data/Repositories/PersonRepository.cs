﻿using AGL.AnimalParser.Data.Entities;
using AGL.AnimalParser.Models.Dtos;
using AGL.AnimalParser.Models.Enums;
using AGL.AnimalParser.Models.Helpers;
using Newtonsoft.Json;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AGL.AnimalParser.Data.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        public PersonRepository(ILogger logger)
        {
            _logger = logger.ForContext<PersonRepository>();
        }

        private readonly ILogger _logger;

        /// <summary>
        /// This should be stored in config of solution.
        /// </summary>
        private const string PeopleExternalServiceApiBaseUrl = "http://agl-developer-test.azurewebsites.net";

        /// <summary>
        /// This should be stored in config of solution.
        /// </summary>
        private const string PeopleExternalServiceApiResource = "people.json";

        /// <summary>
        /// This should be a proper caching implementation...
        /// </summary>
        private static List<PersonDto> _cache = default(List<PersonDto>);

        public async Task<List<PersonDto>> GetAllPeople()
        {
            if (_cache == default(List<PersonDto>))
            {
                _logger.Verbose("Loading all people from data source into cache.");

                var entities = await LoadPeopleFromExternalService();

                // Instead of a cast, we really should take advantage of a library like AutoMapper to map data layer entities to DTO objects to send to clients
                _cache = entities.Select(e => (PersonDto)e).ToList();
            }
            else
            {
                _logger.Verbose("Getting all people from cache.");
            }

            return _cache;
        }

        public HashSet<PetType> GetAllPetTypes()
        {
            return EnumHelper.GetAllEnumValues<PetType>();
        }

        private async Task<List<Person>> LoadPeopleFromExternalService()
        {
            using (WebClient client = new WebClient())
            {
                _logger.Verbose("Loading all people from {ApiRequestUrl}.", string.Join("/", PeopleExternalServiceApiBaseUrl, PeopleExternalServiceApiResource));

                client.BaseAddress = PeopleExternalServiceApiBaseUrl;

                string response;
                try
                {
                    response = await client.DownloadStringTaskAsync(PeopleExternalServiceApiResource);
                }
                catch (WebException ex)
                {
                    _logger.Error(ex, "Request to load people failed.");
                    throw;
                }

                var result = JsonConvert.DeserializeObject<List<Person>>(response);

                return result ?? new List<Person>();
            }
        }
    }
}