﻿using Serilog;
using System.IO;

namespace AGL.AnimalParser.Logging
{
    public static class LoggingConfig
    {
        public const string LogFileName = "AGL.AnimalParser.txt";

        public static LoggerConfiguration GetConfig(string rollingFilePath)
        {
            var config = new LoggerConfiguration();

            config.MinimumLevel.Verbose();

            // Rolling log file
            string template = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] [{SourceContext:l}] {Message}{NewLine}{Exception}";
            config.WriteTo.File(
                Path.Combine(rollingFilePath, LogFileName),
                outputTemplate: template,
                shared: true,
                fileSizeLimitBytes: 5242880 // 5MB
            );

            return config;
        }
    }
}