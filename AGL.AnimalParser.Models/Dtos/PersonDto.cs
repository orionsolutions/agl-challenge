﻿using AGL.AnimalParser.Models.Enums;
using System.Collections.Generic;

namespace AGL.AnimalParser.Models.Dtos
{
    public class PersonDto
    {
        public string Name { get; set; }

        public PersonGender Gender { get; set; }

        public int Age { get; set; }

        public IList<PetDto> Pets { get; set; }
    }
}