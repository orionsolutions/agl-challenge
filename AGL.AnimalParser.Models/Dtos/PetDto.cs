﻿using AGL.AnimalParser.Models.Enums;

namespace AGL.AnimalParser.Models.Dtos
{
    public class PetDto
    {
        public string Name { get; set; }

        public PetType Type { get; set; }
    }
}