﻿namespace AGL.AnimalParser.Models.Enums
{
    public enum PersonGender
    {
        Male,
        Female
    }
}