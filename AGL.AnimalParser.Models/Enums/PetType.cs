﻿namespace AGL.AnimalParser.Models.Enums
{
    public enum PetType
    {
        Cat,
        Dog,
        Fish
    }
}