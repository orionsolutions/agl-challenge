﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AGL.AnimalParser.Models.Helpers
{
    public static class EnumHelper
    {
        public static HashSet<TEnum> GetAllEnumValues<TEnum>() where TEnum : struct, IComparable, IConvertible, IFormattable
        {
            return new HashSet<TEnum>(Enum.GetValues(typeof(TEnum)).Cast<TEnum>());
        }
    }
}