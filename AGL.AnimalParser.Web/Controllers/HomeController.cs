﻿using AGL.AnimalParser.Application.Services;
using AGL.AnimalParser.Logging;
using AGL.AnimalParser.Models.Enums;
using AGL.AnimalParser.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Serilog;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AGL.AnimalParser.Web.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IPersonAppService personAppService, ILogger logger)
        {
            _personAppService = personAppService;
            _logger = logger.ForContext<HomeController>();
        }

        private readonly ILogger _logger;

        private readonly IPersonAppService _personAppService;

        public async Task<IActionResult> Index([FromQuery(Name = "t")]PetType petTypeOfInterest = PetType.Cat)
        {
            var grouped = await _personAppService.GroupAllPetsByGenderAndOrderByPetName(petTypeOfInterest);

            var vm = new AnimalGroupingViewModel()
            {
                PetTypeOfInterest = petTypeOfInterest,
                Pets = grouped,
                PetInterestOptions = _personAppService.GetAllPetTypes().Select(t => new SelectListItem()
                {
                    Selected = t == petTypeOfInterest,
                    Text = t.ToString(),
                    Value = t.ToString()
                }).ToList()
            };

            _logger.Verbose("Returned view.");

            return View(vm);
        }

        public async Task<IActionResult> GetLogs()
        {
            // Hacky but done to expose logs dangerously
            string logPath = Path.Combine(Startup.LogDirectory, LoggingConfig.LogFileName);
            if (!System.IO.File.Exists(logPath)) return Json(new { Error = "No log file exists yet." });

            var fs = new FileStream(logPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return File(fs, "text/plain", LoggingConfig.LogFileName);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}