﻿using AGL.AnimalParser.Models.Dtos;
using AGL.AnimalParser.Models.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace AGL.AnimalParser.Web.Models
{
    public class AnimalGroupingViewModel
    {
        public PetType PetTypeOfInterest { get; set; }

        public Dictionary<PersonGender, List<PetDto>> Pets { get; set; }

        public List<SelectListItem> PetInterestOptions { get; set; }
    }
}