﻿using AGL.AnimalParser.Application.Services;
using AGL.AnimalParser.Data.Repositories;
using AGL.AnimalParser.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.IO;

namespace AGL.AnimalParser.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSingleton<ILogger>(GetSerilogConfig().CreateLogger());
            services.AddSingleton<IPersonRepository, PersonRepository>();
            services.AddTransient<IPersonAppService, PersonAppService>();

            // We could also integrate Serilog in .Net Core's logging pipeline but I won't add noise to the logs :)
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        internal static readonly string LogDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Logs");

        private LoggerConfiguration GetSerilogConfig()
        {
            // Should write to a friendly log viewer like https://getseq.net

            // wwwroot isn't a good location for logs at all... but storing here so you can view them easily in Azure hosted app
            return LoggingConfig.GetConfig(LogDirectory);
        }
    }
}