# AGL coding challenge

## Frameworks
- .NET Standard 2.0 (Class library projects).
- .NET Core 2.0 (Web / MVC project).

## Consumed libraries
- Newtonsoft.Json v10.0.3 (for deserialising requests to AGL Web API).
- Serilog v2.6.0 + Serilog.Sinks.File v4.0.0 (for logging).
- Any libraries included with default ASP.NET Core Visual Studio template.

## Notes
- See [here](https://github.com/restsharp/RestSharp/issues/1010) for reason why System.Net.WebClient used instead of RestSharp.
- For the sake of extensibility, `PetType` and `PersonGender` enum values can be modified to match data source changes without affecting operation or tests.
- Rolling logs are located at AGL.AnimalParser.Web\wwwroot\Logs. See `Startup.cs` on why logs are stored here.
- Hosted as Azure App service accessible at http://aglanimalparser.azurewebsites.net.
